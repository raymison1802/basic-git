import unittest from calculator import Calculator

class TestCalculatorMethods (unittest.TestCase):

def setUp(self):

"""Setup the Calculator object that can be used in the tests."""

self.calc Calculator()

def test_add(self): result self.calc.add(2, 3) self.assertEqual(result, 5)

def test_subtract(self): result = self.calc.subtract (5, 3) self.assertEqual(result, 2)

def test_multiply(self): result self.calc.multiply(2, 3) self.assertEqual(result, 6)

def test_divide(self): result = self.calc.divide(6, 2) self.assertEqual(result, 3)

def test_divide_by_zero(self):

"""Test if the Calculator raises a ValueError when dividing by zero. with self.assertRaises (ValueError): self.calc.divide(1, 0)

if _name_ == "_main__": unittest.main()